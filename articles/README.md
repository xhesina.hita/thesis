# references

This project contains some papers (see the following table) on image classification. 


Title
---|
[Method for Diagnosis of Acute Lymphoblastic Leukemia Based on ViT-CNN Ensemble Model](https://www.hindawi.com/journals/cin/2021/7529893/)
[Bayesian Convolutional Neural Network-based Models for Diagnosis of Blood Cancer](https://www.tandfonline.com/doi/full/10.1080/08839514.2021.2011688)
[Automated blast cell detection for Acute Lymphoblastic Leukemia diagnosis](https://shura.shu.ac.uk/28683/)
[Transformers in Medical Image Analysis: A Review](https://www.sciencedirect.com/science/article/pii/S2667102622000717)
[Transfer learning for medical images analyses: A Survey ](https://www.sciencedirect.com/science/article/abs/pii/S0925231222003174)
[A survey on transfer learning](https://ieeexplore.ieee.org/document/5288526)


